var MongoConfig = require('./../config/mongo/mongo-config');
var MongooseQueryException = require('./../exceptions/mongoose-query.exception').MongooseQueryException;
var CONSTANT = require('./../constant/appConstant');
var MODEL = require('./../config/mongo/record.model.js');

// IMPLEMENT PAGINATION AT db FETCH LEVEL.. : done
// CONNECTION POOL : done
// CHECK FOR PERFORMANCE CHECK: done
// SERVER SIDE SEARCHING: done
// SERVER SIDE SORTING : done
// create separate record for user: done

/**
 * Get all records
 * @param req
 * @param res
 */
exports.getAll = function (req, res) {
    var mongoose = MongoConfig.getConnection();
    var Record = mongoose.model('Record', MODEL.Record);

    var pageNo = req.query['page_no'];
    var pageSize = req.query['page_size'];
    var search = req.query.search;
    var sortOrder = (req.query.sort && req.query.sort.charAt(0) === '-') ? CONSTANT.SORT_ORDER.DESCENDING : CONSTANT.SORT_ORDER.ASCENDING;
    var sort = (req.query.sort && req.query.sort.charAt(0) === '-') ? req.query.sort.slice(1) : req.query.sort;
    var filtr = req.query.filter;

    /**
     * Prepare sort query
     * @type {{}}
     */
    var sortQuery = {};
    if(sort === 'title'){
        sort ? sortQuery['normalizedTitle'] = sortOrder : {};
    }

    var response = {};
    response.meta = {};

    //todo: its doing exact search, make it like 'like'
    var query = search ? {title: new RegExp(search, 'i')} : {};
    //var query = search ? {title: search} : {};
    var fields = {};

    try {
        //throw new MongooseQueryException(CONSTANT.EXCEPTION.MESSAGES.MONGOOSE_QUERY_EXCEPTION)
        /**
         * Get total records from Record
         */
        Record.count(query, function (err, count) {
            if (err) {
                throw new MongooseQueryException(CONSTANT.EXCEPTION.MESSAGES.MONGOOSE_QUERY_EXCEPTION)
            } else {
                console.log('--count: ', count);
                var skipFactor = parseInt(((pageNo - 1) * pageSize));
                var limitFactor = parseInt(pageSize);
                console.log('-query: ', query);
                console.log('-sortQuery: ', sortQuery);
                /**,
                 * Get paginated records
                 */
                var preparedQuery = Record.find(query, fields, {skip: skipFactor, limit: limitFactor}).sort(sortQuery);
                preparedQuery.exec(function (err, resultantRecords) {
//                    throw new MongooseQueryException(CONSTANT.EXCEPTION.MESSAGES.MONGOOSE_QUERY_EXCEPTION)
                    if (err) {
                        throw new MongooseQueryException(CONSTANT.EXCEPTION.MESSAGES.MONGOOSE_QUERY_EXCEPTION)
                    } else {
                        response.meta.totalRecords = count;
                        response.meta.totalPages = Math.ceil(response.meta.totalRecords / pageSize);
                        response.meta.currentPage = parseInt(pageNo);
                        response.data = resultantRecords;
                        //console.log('#result:: ', resultantRecords);
                        MongoConfig.closeConnection(mongoose);
                        res.status(CONSTANT.STATUS_CODE.OK).send(response);
                    }
                });
            }
        });
    } catch (exception) {
        MongoConfig.closeConnection(mongoose);
        console.log(CONSTANT.EXCEPTION.PREFIX + ': ' + exception.name + ': ' + exception.message);
        res.status(CONSTANT.STATUS_CODE.INTERNAL_SERVER_ERROR).send(response);
    } finally {

    }
};

/**
 * Delete record for given id
 *
 * @param req
 * @param res
 */
exports.delete = function (req, res) {
    var mongoose = MongoConfig.getConnection();
    var Record = mongoose.model('Record', MODEL.Record);

    var id = req.params.id;

    try {
        Record.remove({_id: id}, function (err, result) {
            if (err) {
                throw new MongooseQueryException(CONSTANT.EXCEPTION.MESSAGES.MONGOOSE_QUERY_EXCEPTION)
            } else {
                MongoConfig.closeConnection(mongoose);
                res.status(CONSTANT.STATUS_CODE.OK).send();
            }
        })
    } catch (exception) {
        MongoConfig.closeConnection(mongoose);
        console.log(CONSTANT.EXCEPTION.PREFIX + ': ' + exception.name + ': ' + exception.message);
        res.status(CONSTANT.STATUS_CODE.INTERNAL_SERVER_ERROR).send(response);
    }
};

/**
 * Insert new record
 * @param req
 * @param res
 */
exports.insert = function (req, res) {
    var mongoose = MongoConfig.getConnection();
    var Record = mongoose.model('Record', MODEL.Record);

    var record = new Record(req.body);
    record.normalizedTitle = record.title.toLowerCase();

    try {
        record.save(function (err, result) {
            if (err) {
                throw new MongooseQueryException(CONSTANT.EXCEPTION.MESSAGES.MONGOOSE_QUERY_EXCEPTION)
            } else {
                MongoConfig.closeConnection(mongoose);
                res.status(CONSTANT.STATUS_CODE.CREATED).send(result);
            }
        })
    } catch (exception) {
        MongoConfig.closeConnection(mongoose);
        console.log(CONSTANT.EXCEPTION.PREFIX + ': ' + exception.name + ': ' + exception.message);
        res.status(CONSTANT.STATUS_CODE.INTERNAL_SERVER_ERROR).send();
    }
};

/**
 * Update existing record
 * @param req
 * @param res
 */
exports.update = function (req, res) {
    var mongoose = MongoConfig.getConnection();
    var Record = mongoose.model('Record', MODEL.Record);


    console.log('--update: ', req.body);
    var query = {$set: {title: req.body.title, data: req.body.data}};

    try {
        Record.update({_id: req.body._id}, query, function (err, result) {
            if (err) {
                throw new MongooseQueryException(CONSTANT.EXCEPTION.MESSAGES.MONGOOSE_QUERY_EXCEPTION)
            } else {
                MongoConfig.closeConnection(mongoose);
                res.status(CONSTANT.STATUS_CODE.OK).send();
            }
        })
    } catch (exception) {
        MongoConfig.closeConnection(mongoose);
        console.log(CONSTANT.EXCEPTION.PREFIX + ': ' + exception.name + ': ' + exception.message);
        res.status(CONSTANT.STATUS_CODE.INTERNAL_SERVER_ERROR).send();
    }
};
