var CONSTANT = require('./../constant/appConstant');
var MongoConfig = require('./../config/mongo/mongo-config');
var MODEL = require('./../config/mongo/user.model');

exports.login = function (req, res, next) {

    var mongoose = MongoConfig.getConnection();
    var User = mongoose.model('User', MODEL.User);

    /*
     var admin = new User({userName: 'admin', password: 'admin', isDBA: true});
     var user = new User({userName: 'user', password: 'user', isDBA: false});

     admin.save(function(){});
     user.save(function(){});
     */

    var response = {};
    var statusCode = '';
    User.find(req.body, function (err, result) {
        if (err) {
            MongoConfig.closeConnection(mongoose);
            statusCode = CONSTANT.STATUS_CODE.INTERNAL_SERVER_ERROR;
            res.status(statusCode).send(response);
        } else {
            MongoConfig.closeConnection(mongoose);
            if (result.length) {
                response.isDBA = result[0].isDBA;
                statusCode = CONSTANT.STATUS_CODE.OK;
                res.status(statusCode).send(response);
            } else {
                statusCode = CONSTANT.STATUS_CODE.UNAUTHORIZED;
                res.status(statusCode).send(response);
            }
        }
    });


    console.log('---req: ', req.body);
    /*    switch (req.body.userName){
     case 'admin':
     response.isDBA = true;
     statusCode = CONSTANT.STATUS_CODE.OK;
     break;
     case 'user':
     response.isDBA = false;
     statusCode = CONSTANT.STATUS_CODE.OK;
     break;
     default:
     statusCode = CONSTANT.STATUS_CODE.UNAUTHORIZED;
     break;
     }
     res.status(statusCode).send(response);*/
};

