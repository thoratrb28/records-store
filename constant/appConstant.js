module.exports = Object.freeze({
    MONGO: {
        BASE_URL: 'mongodb://localhost/',
        DATABASE: {
            RECORD_STORE: 'record-store'
        }
    },
    SORT_ORDER: {
        ASCENDING: 'ascending',
        DESCENDING: 'descending'
    },
    STATUS_CODE: {
        OK: 200,
        CREATED: 201,
        UNAUTHORIZED: 401,
        INTERNAL_SERVER_ERROR: 500
    },
    EXCEPTION: {
        PREFIX: '$Exception',
        MESSAGES: {
            MONGOOSE_QUERY_EXCEPTION: 'Mongoose query failed'
        }
    }
});