var mongoose = require('mongoose');
var CONSTANT = require('./../../constant/appConstant');

//set pool size to 10
var options = {
    server: {poolSize: 10}
};

/**
 * Get new instance of mongodb
 */
exports.getConnection = function () {
    var url = CONSTANT.MONGO.BASE_URL + CONSTANT.MONGO.DATABASE.RECORD_STORE;
    return mongoose.createConnection(url, options);
};

exports.closeConnection = function (connection) {
    connection.close();
    console.log('closing connection')
};