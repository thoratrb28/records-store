Records Store
================
This repository contains the Service part of 'Records Store'

## Environment
NodeJS

##Database used
MngoDB

## Table of contents

* [NodeJs Installation](#NodeJsInstallation)
* [MongoDB Installation](#MongoDBInstallation)
* [Run application](#Run)
* [Server Monitor](#Server-Monitor)
* [Sample API](#Sample-API)

## NodeJs installation


0. Extract the Folder

0. NodeJS - Install latest stable version for your OS, [NodeJS](https://nodejs.org/en/)

## MongoDB Setup
0. Install latest stable version for your OS, [MongoDB](https://www.mongodb.com/download-center#community)
1.  In one terminal:  `mongod.exe --dbpath "C:\Program Files\MongoDB\data" `
2. In another termial: `mongo.exe`



## Run
>Fire below commands from the root location of the project folder

0. `npm install`
1.  `npm install -g nodemon`
0. `nodemon app.js`

## Server Monitor
After running the application, monitor should be up over http://localhost:5001/

## Sample API

**1. Create record:**
  >***request:***

        Request URL: http://localhost:5000/records
        Request Method: POST
        Data: title=23&data=Sample


  > ***response:***

        success
            Status Code:201 Created
            response: {"__v":0,"title":"23","data":"Sample","_id":"576cd6f3eeafb7181a0c7b21"}


**2. Update record:**
   >***request:***

        Request URL:http://localhost:5000/records
        Request Method:PUT
        data: __v=0&title=233&data=Sample&_id=576cd4754bc545ec31e0dac3&%24%24hashKey=object%3A46


  > ***response:***

        success:
            Status Code:200 OK
            response: {}


**3. Delete record**
   >***request:***

        Request URL:http://localhost:5000/records/576cd4754bc545ec31e0dac3
        Request Method:DELETE


  > ***response:***

        success:
            Status Code:200 OK
            response: {}


**4. Get records**
   >***request:***

        Request URL:http://localhost:5000/records?page_no=1&page_size=10
        Request Method:GET


  > ***response:***

        success:
            Status Code:304 Not Modified
            response:
            {"meta":{"totalPages":22},"data":[{"_id":"576cbca5ab26dd6813a52939","title":"1","data":"","__v":0},{"_id":"576cc2bcbff4919836a6ea6c","title":"2","data":"","__v":0},{"_id":"576cc365bff4919836a6ea6f","title":"3","data":"","__v":0},{"_id":"576cc36abff4919836a6ea70","title":"4","data":"","__v":0},{"_id":"576cc36dbff4919836a6ea71","title":"5","data":"","__v":0},{"_id":"576cc373bff4919836a6ea72","title":"6","data":"","__v":0},{"_id":"576cc377bff4919836a6ea73","title":"7","data":"","__v":0},{"_id":"576cc37cbff4919836a6ea74","title":"8","data":"","__v":0},{"_id":"576cc380bff4919836a6ea75","title":"9","data":"","__v":0},{"_id":"576cc383bff4919836a6ea76","title":"10","data":"","__v":0}]}




## Performance Monitor:
     look: https://www.npmjs.com/package/monitornode