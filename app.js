require('look').start(5001, '127.0.0.1');

var express = require('express'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    records = require('./routes/records'),

    login = require('./routes/login'),
    path = require('path'),
    app = express();

//
require('./performance/performance')
//

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(methodOverride());      // simulate DELETE and PUT

/**
 * Set CORS headers
 */
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type");
    res.header("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT");
    next();
});

/**
 * Handle request
 */
app.get('/', function (req, res) {
    res.send("'Record Store' server is up...");
});
app.post('/login', login.login);
app.get('/records', records.getAll);
app.delete('/records/:id', records.delete);
app.post('/records', records.insert);
app.put('/records', records.update);


/**
 * Set port
 */
app.set('port', process.env.PORT || 5000);


/**
 * Make server up on register port
 */
app.listen(app.get('port'), function () {
    console.log('server up @ ' + app.get('port'));
});


var exports = module.exports = app;
