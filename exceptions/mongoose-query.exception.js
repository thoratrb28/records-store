/**
 * Mongoose Query Exception
 * @param message
 * @param level
 * @constructor
 */
exports.MongooseQueryException = function (message, level) {
    this.name = 'Mongoose Query Exception';
    this.message = message;
    this.level = level || 0;
};